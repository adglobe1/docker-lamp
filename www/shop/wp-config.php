<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
require_once(ABSPATH . 'wp-config-include.php');
set_time_limit(300);
define( 'WP_MEMORY_LIMIT', '256M' );


// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'develbmt_deporteswordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'mysql');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8NYeySh|%Cg)tuHaSK&(,jCAtuqF`wIIrBM5OO;Zi;?aPCA]E&F+|0tke^M,xt5-');
define('SECURE_AUTH_KEY',  '|z/1:W5{-s{R7T=z2Y-]EDG(yDuWql(zj%(K6c8|w*gZ!C)n=!2wH<XnwtB)x{sB');
define('LOGGED_IN_KEY',    ')P?^V[Rez~5[Ka,Ub!E0*2Z7Ve.B]@,u[tTgxi=kr?2gK5Fp<r~uaj{FXBd70C`T');
define('NONCE_KEY',        'I+{(]Ga|bWH:wom{r:%F{gu%^^T_tu~f-,03x}%r:HgHXmssAI4-w%^Egp4b&#R`');
define('AUTH_SALT',        '&vh5n_e37ux4hJq;.P;,i2T+(P/7{t9YEGy%;Csg~n7kJ47(UnRg8e|fz)e+J0=9');
define('SECURE_AUTH_SALT', 'zI@E>^]15=IE%{d~qU7d{z$RV6<+81PcE>rj#U0kI2!1VlVmjcq~8<<D]3j`ma2r');
define('LOGGED_IN_SALT',   'lY6:6pxPB?f%CH%t^T)f]Em=Bg7ST`d,c?jl&YUgnpH)5#ORr<}rYL/go}IYjgFN');
define('NONCE_SALT',       'yF-0>B[DPtPKTqyc$Bo7$a<RHt.5Xcv_ .$Q~zg{aFHgTJ6VtEo3!eFFYmVmDz~^');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('FORCE_SSL_ADMIN', true);
define('FORCE_SSL_LOGIN', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
